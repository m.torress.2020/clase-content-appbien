import webapp

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_404 = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Page not found: {resource}.</p>
  </body>
</html>
"""

class ContentApp(webapp.webApp): # es la hija de webapp

    text = {'/': "<p>Main page</p>",
                '/hello': "<p>Hi</p>",
                '/bye': "<p>Bye bye</p>"}

    def parse (self, request):
        return request.split(' ',2)[1]

    def process (self, resource):
        if resource in self.text:
            content = self.text[resource]
            page = PAGE.format(content=content)
            code = "200 OK"
        else:
            page = PAGE_404.format(resource=resource)
            code = "404 Error: Not Found"
        return (code, page)

if __name__ == "__main__":
    webApp = ContentApp ("localhost", 8888)
